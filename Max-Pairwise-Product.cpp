#include <iostream>
#include <vector>
#include <algorithm>

long long MaxPairwiseProduct(std::vector<int>& numbers) {
    int n = numbers.size();

    sort(numbers.begin(), numbers.end());

    if(n == 1) return numbers[0];
    else return ((long long)numbers[n-1])*numbers[n-2];
}

int main() {
    int n;
    std::cin >> n;
    std::vector<int> numbers(n);
    for (int i = 0; i < n; ++i) {
        std::cin >> numbers[i];
    }

    std::cout << MaxPairwiseProduct(numbers) << "\n";
    return 0;
}
