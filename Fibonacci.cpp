#include <iostream>
#include <cassert>

int fibonacci_naive(int n) {
    if (n <= 1)
        return n;

    return fibonacci_naive(n - 1) + fibonacci_naive(n - 2);
}

//call as int fibonacci_fast(int n, int fib[])
int fibonacci_fast(int n, int *fib) {
    if(n <= 1) return n;
    
    for(int i = 2; i <= n; i++) fib[i] = fib[i-1] + fib[i-2];

    return fib[n];
}

int main() {
    int n = 0;
    std::cin >> n;
    
    int fib[n];
    fib[0] = 0;
    fib[1] = 1;

    std::cout << fibonacci_naive(n) << '\n';
    std::cout << fibonacci_fast(n, fib) << '\n';
    return 0;
}
